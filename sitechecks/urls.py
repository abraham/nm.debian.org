from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.List.as_view(), name="sitechecks_list"),
]
