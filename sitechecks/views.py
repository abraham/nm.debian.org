from django.shortcuts import render
from django.views.generic import TemplateView
from backend.mixins import VisitorMixin
from nm2.lib import assets
from .models import Inconsistency


class List(VisitorMixin, TemplateView):
    assets = [assets.DataTablesBootstrap4]
    template_name = "sitechecks/list.html"
    require_visitor = "admin"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["inconsistencies"] = Inconsistency.objects.all().order_by("-last_seen", "person", "process", "tag", "text")
        return ctx
