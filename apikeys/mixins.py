from __future__ import annotations
from backend.mixins import VisitorMixin


class APIVisitorMixin(VisitorMixin):
    """
    Allow to use api keys to set visitor information
    """
    pass
