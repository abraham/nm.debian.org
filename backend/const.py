from collections import namedtuple
from django.utils.translation import gettext_lazy as _

Status = namedtuple("Status", ("code", "tag", "sdesc", "pronoun", "ldesc"))
Progress = namedtuple("Progress", ("code", "tag", "sdesc", "ldesc"))

g = globals()

# Status of a person in Debian
ALL_STATUS = (
    Status("STATUS_DC",            "dc",      "DC",            _("a"), _("Debian Contributor")),
    Status("STATUS_DC_GA",         "dc_ga",   "DC+account",    _("a"), _("Debian Contributor, with guest account")),
    Status("STATUS_DM",            "dm",      "DM",            _("a"), _("Debian Maintainer")),
    Status("STATUS_DM_GA",         "dm_ga",   "DM+account",    _("a"), _("Debian Maintainer, with guest account")),
    Status("STATUS_DD_U",          "dd_u",    "DD, upl.",      _("a"), _("Debian Developer, uploading")),
    Status("STATUS_DD_NU",         "dd_nu",   "DD, non-upl.",  _("a"), _("Debian Developer, non-uploading")),
    Status("STATUS_EMERITUS_DD",   "dd_e",    "DD, emeritus",  _("a"), _("Debian Developer, emeritus")),
    Status("STATUS_REMOVED_DD",    "dd_r",    "DD, removed",   _("a"), _("Debian Developer, removed")),
)
ALL_STATUS_DESCS = dict((x.tag, x.ldesc) for x in ALL_STATUS)
ALL_STATUS_DESCS_WITH_PRONOUN = dict(
    (x.tag, '{} {}'.format(x.pronoun, x.ldesc)) for x in ALL_STATUS)
ALL_STATUS_BYTAG = dict((x.tag, x) for x in ALL_STATUS)
for s in ALL_STATUS:
    g[s.code] = s.tag

SEQ_STATUS = dict(((y.tag, x) for x, y in enumerate(ALL_STATUS)))

# Progress of a person in a process
ALL_PROGRESS = (
    Progress("PROGRESS_APP_NEW",   "app_new",   _("Applied"),     _("Applicant asked to enter the process")),
    Progress("PROGRESS_APP_RCVD",  "app_rcvd",  _("Validated"),   _("Applicant replied to initial mail")),
    Progress("PROGRESS_APP_HOLD",  "app_hold",  _("App hold"),    _("On hold before entering the queue")),
    Progress("PROGRESS_ADV_RCVD",  "adv_rcvd",  _("Adv ok"),      _("Received enough advocacies")),
    Progress("PROGRESS_POLL_SENT", "poll_sent", _("Poll sent"),   _("Activity poll sent")),
    Progress("PROGRESS_APP_OK",    "app_ok",    _("App ok"),      _("Advocacies have been approved")),
    Progress("PROGRESS_AM_RCVD",   "am_rcvd",   _("AM assigned"), _("Waiting for AM to confirm")),
    Progress("PROGRESS_AM",        "am",        _("AM"),          _("Interacting with an AM")),
    Progress("PROGRESS_AM_HOLD",   "am_hold",   _("AM hold"),     _("AM hold")),
    Progress("PROGRESS_AM_OK",     "am_ok",     _("AM ok"),       _("AM approved")),
    Progress("PROGRESS_FD_HOLD",   "fd_hold",   _("FD hold"),     _("FD hold")),
    Progress("PROGRESS_FD_OK",     "fd_ok",     _("FD ok"),       _("FD approved")),
    Progress("PROGRESS_DAM_HOLD",  "dam_hold",  _("DAM hold"),    _("DAM hold")),
    Progress("PROGRESS_DAM_OK",    "dam_ok",    _("DAM ok"),      _("DAM approved")),
    Progress("PROGRESS_DONE",      "done",      _("Done"),        _("Completed")),
    Progress("PROGRESS_CANCELLED", "cancelled", _("Cancelled"),   _("Cancelled")),
)
ALL_PROGRESS_DESCS = dict((x.tag, x.ldesc) for x in ALL_PROGRESS)
ALL_PROGRESS_BYTAG = dict((x.tag, x) for x in ALL_PROGRESS)
for p in ALL_PROGRESS:
    g[p.code] = p.tag

SEQ_PROGRESS = dict(((y.tag, x) for x, y in enumerate(ALL_PROGRESS)))
