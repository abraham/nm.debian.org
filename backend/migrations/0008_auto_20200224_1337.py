# Generated by Django 2.2.9 on 2020-02-24 13:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0007_fullname'),
        ('dsa', '0002_fill_ldap'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='person',
            options={'ordering': ('fullname',)},
        ),
        migrations.RemoveField(
            model_name='person',
            name='cn',
        ),
        migrations.RemoveField(
            model_name='person',
            name='email_ldap',
        ),
        migrations.RemoveField(
            model_name='person',
            name='mn',
        ),
        migrations.RemoveField(
            model_name='person',
            name='sn',
        ),
        migrations.RemoveField(
            model_name='person',
            name='uid',
        ),
    ]
