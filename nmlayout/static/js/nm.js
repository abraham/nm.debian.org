(function($) {
"use strict";

// Install fingerprint easy-copy-paste behaviour
function install_fingerprint_widget()
{
    $("span.fpr").each(function(idx, el) {
        var el = $(el);
        var joined = $("<input>").attr("type", "text").attr("size", "40").val(el.text().replace(/ /g,""));
        el.after(joined);
        joined.hide();
        el.click(function(ev) {
            el.hide();
            joined.show();
            joined.focus();
        });
        joined.focusout(function(ev) {
            el.show();
            joined.hide();
        });
    });
}

function install_person_bio_widget()
{
    $("div.personbio").click(function (ev) {
        var el = $(this);
        el.find(".collapsed").toggle();
        el.find(".expanded").toggle();
    });
}

function get_cookie(name) {
    var cookie_value = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookie_value = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookie_value;
}

// See https://webdesign.tutsplus.com/tutorials/how-to-make-the-bootstrap-navbar-dropdown-work-on-hover--cms-33840
class NavbarHover
{
    constructor(dropdown)
    {
        this.dropdown = $(dropdown);
        this.dropdown_toggle = this.dropdown.find(".dropdown-toggle");
        this.dropdown_menu = this.dropdown.find(".dropdown-menu");
        $(window).on("resize", evt => { this.refresh(); });
        this.refresh();
    }

    hover_in(evt)
    {
        this.dropdown.addClass("show");
        this.dropdown_toggle.attr("aria-expanded", "true");
        this.dropdown_menu.addClass("show");
    }

    hover_out(evt)
    {
        this.dropdown.removeClass("show");
        this.dropdown_toggle.attr("aria-expanded", "false");
        this.dropdown_menu.removeClass("show");
    }

    refresh()
    {
        if (window.matchMedia("(min-width: 768px)").matches)
        {
            this.dropdown.hover(
                evt => { this.hover_in(evt); },
                evt => { this.hover_out(evt); });
        } else {
            this.dropdown.off("mouseenter mouseleave");
        }
    }

    static install()
    {
        $("#main-navbar1").find(".dropdown").each((idx, el) => {
            new NavbarHover(el);
        });
    }
}


class StatusInfo
{
    constructor(entry, sequence)
    {
        this.label = entry[0];
        this.name = entry[1];
        this.sdesc = entry[2];
        this.pronoun = entry[3];
        this.ldesc = entry[4];
        this.sequence = sequence;
    }
}

class API
{
    constructor() {
        this.csrf_token = window.nm2.csrf_token;
    }

    async _fetch_json(name, args, url, init)
    {
        let response;
        try {
            response = await fetch(url, init);
        } catch (error) {
            console.log("API GET ERROR", {
                name: name,
                args: args,
                url: url,
                init: init,
                msg: error.message,
                response: response,
            });
            throw error;
        }

        const content_type = response.headers.get("Content-Type");
        if (!content_type || !content_type.includes("application/json"))
        {
            const text = await response.text();
            const errmsg = `content-type is '${content_type}' instead of application/json.`;
            console.log("API GET ERROR", {
                name: name, 
                args: args,
                url: url,
                init: init,
                msg: errmsg,
                response: response,
                body: text,
            });
            throw new Error(errmsg);
        }

        const json = await response.json();

        if (!response.ok)
        {
            const errmsg = `response result: ${response.status} ${response.statusText}`;
            console.log("API GET ERROR", {
                name: name,
                args: args,
                url: url,
                init: init,
                msg: errmsg,
                response: response,
                body: json,
            });
            throw new Error(errmsg);
        }

        return json;
    };

    async _get(url, args) {
        let full_url = new URL(url, window.location.href);
        for (const [name, val] of args)
            full_url.searchParams.append(name, val)

        return await this._fetch_json(name, args, full_url, {
            method: "GET",
            mode: "same-origin",
            credentials: "same-origin",
        });
    }

    async _post(url, args) {
        let full_url = new URL(url, window.location.href);
        const data = JSON.stringify(args);
        console.log("_POST", data);
        return await this._fetch_json(name, args, full_url, {
            method: "POST",
            mode: "same-origin",
            credentials: "same-origin",
            headers: new Headers({
                "Content-Type": "application/json",
                "Content-Length": data.length,
                "X-XSRFToken": this.xsrf_token,
            }),
            body: data,
        });
    }

    async people(query) {
        return await this._get(window.nm2.url_api_people, query);
    }
}

if (window.nm2 === undefined)
    window.nm2 = {};

window.nm2.csrftoken = get_cookie("csrftoken");
window.nm2.API = API;


function main()
{
    install_fingerprint_widget();
    install_person_bio_widget();
    NavbarHover.install();

    // Initialize message notification
    $(".toast").toast({
        autohide: false,
    }).toast('show');

    // Populate ALL_STATUS
    window.nm2.ALL_STATUS = {};
    let sequence = 0;
    const el = document.getElementById("ALL_STATUS");
    if (el)
        for (const entry of JSON.parse(el.textContent))
            window.nm2.ALL_STATUS[entry[1]] = new StatusInfo(entry, sequence++);
}

document.addEventListener("DOMContentLoaded", evt => { main(); });

})(jQuery);
